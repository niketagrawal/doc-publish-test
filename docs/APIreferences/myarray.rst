My array
=======================

.. automodule:: apidoctestbench.myarray

.. autofunction:: multiplication_table

.. autofunction:: subtract
