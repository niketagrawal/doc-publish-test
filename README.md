Experiment to test API documentation generation with Sphinx for a Python code in a public repository on TU Delft GitLab and host the documentation on Read the Docs. 

Published docs URL: https://doc-publish-test.readthedocs.io/en/latest/